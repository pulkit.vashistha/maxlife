package com.ttn.core.use;

import java.util.List;

public class SubNavComponent {
    private String title;
    private List<SubNavChild> children;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SubNavChild> getChildren() {
        return children;
    }

    public void setChildren(List<SubNavChild> children) {
        this.children = children;
    }
}
