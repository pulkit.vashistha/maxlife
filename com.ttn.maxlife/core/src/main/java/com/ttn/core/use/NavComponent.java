package com.ttn.core.use;

import java.util.List;

public class NavComponent {
    private String title;
    private List<SubNavComponent> children;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SubNavComponent> getChildren() {
        return children;
    }

    public void setChildren(List<SubNavComponent> children) {
        this.children = children;
    }
}
