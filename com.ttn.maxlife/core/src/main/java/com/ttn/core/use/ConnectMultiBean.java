package com.ttn.core.use;

public class ConnectMultiBean {

    private String heading;
    private String contact;
    private String supportmail;
    private String linkhead;
    private String linkurl;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getSupportmail() {
        return supportmail;
    }

    public void setSupportmail(String supportmail) {
        this.supportmail = supportmail;
    }

    public String getLinkhead() {
        return linkhead;
    }

    public void setLinkhead(String linkhead) {
        this.linkhead = linkhead;
    }

    public String getLinkurl() {
        return linkurl;
    }

    public void setLinkurl(String linkurl) {
        this.linkurl = linkurl;
    }
}
