package com.ttn.core.use;

import com.adobe.cq.sightly.WCMUsePojo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONObject;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.day.cq.personalization.offerlibrary.usebean.OffercardPropertiesProvider.LOGGER;
import static com.day.cq.wcm.foundation.List.log;

public class UsePojoChild extends WCMUsePojo {

    private List<ArticlesMultiBean> articleItems = new ArrayList<>();
    private List<NavComponent> navItems = new ArrayList<>();
    private List<Component1> component1Items = new ArrayList<>();

    @Override
    public void activate() throws Exception {
//        name = get("name",String.class);
        if(getResource().getName().equals("header"))
        setNavComponents();
        else
        setArticleComponents();
        //setNavComponent1();


    }

    private List<Component1> setNavComponent1() throws Exception{
        Node components1 = getResource().adaptTo(Node.class);
        NodeIterator n = components1.getNodes();

        while(n.hasNext()){
            Component1 component1 = new Component1();
            Node node = n.nextNode();
            component1.setTitle(node.getProperty("navTitle").toString());
            component1.setUrl(node.getProperty("navUrl").toString());

            component1Items.add(component1);
        }
        return component1Items;
    }

    public List<Component1> getNavComponent1() {
        return component1Items;
    }


    private List<NavComponent> setNavComponents() throws Exception{

        if(getResource().getChild("NavComponents")!=null) {

            Node navComponents = getResource().getChild("NavComponents").adaptTo(Node.class);

            NodeIterator n1 = navComponents.getNodes();

            log.info("before n1.hasNext");
            while (n1.hasNext()) {
                log.info("in n1.hasNext");

                NavComponent navComponent = new NavComponent();

                Node node1 = n1.nextNode();
                navComponent.setTitle(node1.getProperty("navTitle").getString());

                if (node1.getNode("./SubNavComponents")!=null) {

                     Node navItem = node1.getNode("./SubNavComponents");
                    NodeIterator n2 = navItem.getNodes();

                    List<SubNavComponent> subNavComponents = new ArrayList<>();

                    log.info("before n2.hasNext");
                    while (n2.hasNext()) {
                        log.info("in n2.hasNext");
                        SubNavComponent subNavComponent = new SubNavComponent();

                        Node node2 = n2.nextNode();
                        subNavComponent.setTitle(node2.getProperty("subNavTitle").getString());

                        if(node2.getNode("./SubNavChildren")!=null) {
                            Node subnavItem = node2.getNode("./SubNavChildren");
                            NodeIterator n3 = subnavItem.getNodes();

                            List<SubNavChild> subNavChildren = new ArrayList<>();

                            log.info("before n3.hasNext");
                            while (n3.hasNext()) {
                                log.info("in n3.hasNext");
                                SubNavChild subNavChild = new SubNavChild();

                                Node subnavchildItem = n3.nextNode();
                                subNavChild.setTitle(subnavchildItem.getProperty("subNavChildren").getString());
                                subNavChild.setUrl(subnavchildItem.getProperty("url").getString());
                                subNavChildren.add(subNavChild);
                            }log.info("finish n3.hasNext");

                            subNavComponent.setChildren(subNavChildren);

                            subNavComponents.add(subNavComponent);

                        }else{

                        }

                    }log.info("finish n2.hasNext");

                    navComponent.setChildren(subNavComponents);

                    navItems.add(navComponent);
                }
                else {

            }

            }
            log.info("finish n1.hasNext");

//
//            while()
//            log.info("Navbar:   :   :",navItems);
            return navItems;

        }
        else{
            return null;
        }

    }

    public List<NavComponent> getNavComponents(){
        return navItems;
    }

    private void setArticleComponents() throws Exception {

        Node currentNode = getResource().getChild("articles").adaptTo(Node.class);

        NodeIterator ni = currentNode.getNodes();
        while (ni.hasNext()) {

            Node child = (Node) ni.nextNode();
            setArticleFieldItems(child);

        }
    }

    private List<ArticlesMultiBean> setArticleFieldItems(Node node) {

        try {

            String title;
            String description;
            String readmore;
            String link;

            //THIS WILL READ THE VALUE OF THE CORAL3 Multifield and set them in a collection

            ArticlesMultiBean menuItem = new ArticlesMultiBean();

            log.info("*** GRAND CHILD NODE PATH IS " + node.getPath());

            title = node.getProperty("articleTitle").getString();
            description = node.getProperty("articleText").getString();
            readmore = node.getProperty("readmore").getString();
            link = node.getProperty("readlink").getString();

//                log.info("*** VALUE OF DATE IS "+startDate);

            menuItem.setTitle(title);
            menuItem.setDescription(description);
            menuItem.setReadmore(readmore);
            menuItem.setLink(link);

            articleItems.add(menuItem);
        } catch (Exception e) {
            log.error("Exception while Multifield data {}", e.getMessage(), e);
        }
        return articleItems;
    }

    public List<ArticlesMultiBean> getMultiFieldItems() {
        return articleItems;
    }

}
